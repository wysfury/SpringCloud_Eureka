package cn.xinagxu.client02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class Client02Application {

	public static void main(String[] args) {
		SpringApplication.run(Client02Application.class, args);
	}
}
